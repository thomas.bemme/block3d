/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;

import android.content.Context;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

public class GameView extends SurfaceView implements Callback {
	private final SurfaceHolder _surfaceHolder;
	private GameThread canvasthread;
	private int mCanvasHeight;
	private int mCanvasWidth;
	private boolean isSurfaceCreated;

	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		_surfaceHolder = getHolder();
		_surfaceHolder.setFormat(PixelFormat.OPAQUE);
		_surfaceHolder.addCallback(this);
		setFocusable(true);

	}

	public GameThread getThread() {
		if (canvasthread == null || !canvasthread.isAlive()) {
			// create thread only; it's started in surfaceCreated()
			canvasthread = new GameThread(getHolder(), getContext());
			updateThreadSurfaceState();
			if (mCanvasHeight > 0 && mCanvasWidth > 0) {
				canvasthread.setSurfaceSize(mCanvasWidth, mCanvasHeight);
			}
		}
		return canvasthread;
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus) {
			if (canvasthread != null) {
				canvasthread.pause();
			}
		} else {
			if (canvasthread != null) {
				canvasthread.unpause();
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		mCanvasWidth = width;
		mCanvasHeight = height;
		canvasthread.setSurfaceSize(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		isSurfaceCreated = true;
		updateThreadSurfaceState();
		if (!hasFocus()) {
			requestFocusFromTouch();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		isSurfaceCreated = false;
		updateThreadSurfaceState();
		stopThread();
		// Log.i("GameView", "surfaceDestroyed");
	}

	private void updateThreadSurfaceState() {
		if (canvasthread != null) {
			canvasthread.setSurfaceCreated(isSurfaceCreated);
		}
	}

	private void stopThread() {
		if (canvasthread != null) {
			// Log.d("GameView", "Stopping Thread");
			boolean retry = true;
			canvasthread.setRunning(false);
			while (retry) {
				try {
					canvasthread.join();
					retry = false;
				} catch (InterruptedException ignored) {
				}
			}
			// Log.d("GameView", "Thread Stopped");
		}
	}

	public void releaseAllResources() {
		// Log.d("GameView", "Releasing Resources");
		if (getHolder() != null) {
			getHolder().removeCallback(this);
		}
		stopThread();
	}

	@Override
    public boolean performClick() {
        return super.performClick();
    }
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		synchronized (_surfaceHolder) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
				canvasthread.releaseButton(x, y);
				//canvasthread.unpause();
				performClick();
				break;
			case MotionEvent.ACTION_DOWN:
				canvasthread.pressButton(x, y);
				break;
			case MotionEvent.ACTION_MOVE:
				canvasthread.moveButton(x, y);
				break;
			default:
				return false;
			}
		}
		return true;
	}

}

