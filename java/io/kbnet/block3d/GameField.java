/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import android.media.AudioManager;

import java.util.Random;

class GameField {
    public static final Bitmap.Config FAST_BITMAP_CONFIG = Bitmap.Config.RGB_565;
    private static final int[] bgcolor = {Color.rgb(0, 0, 255),
            Color.rgb(0, 255, 0), Color.rgb(255, 0, 0), Color.rgb(128, 0, 128),
            Color.rgb(255, 165, 0), Color.rgb(255, 255, 0),
            Color.rgb(255, 0, 255), Color.rgb(0, 255, 255),
            Color.rgb(0, 0, 255), Color.rgb(0, 255, 0)};
    private final int[] recCornersBig = new int[4];
    private final int[] recCornersSmall = new int[4];
    private final int[] q = new int[11];
    public GameThread gameThread;
    private GameValues gameValues;
    private Bitmap help;
    private Bitmap soundon;
    private Bitmap soundoff;
    private boolean moved = false;
    private long lastMove = 0;
    private boolean grounded = false;
    private Rect recField;
    private SoundPool sp;
    private int[] sound = new int[5];
    private Rect soundRect;
    private Paint basePaint;
    private Bitmap neonh;
    private Bitmap neonv;
    private int width;
    private int height;
    private Context context;
    private long lasttouch = 0;
    private float touchx;
    private float touchy;
    private boolean showDialog = true;
    private Rect planesRect;
    private Rect controlFrame;
    private int touchMat = 0;

    public void create(GameValues gv) {
        gameValues = gv;
        int x = gameValues.getStage();
        int y = x;
        int z = 10;
        q[10] = 0;
        if (gameValues.points == 0) {
            gameValues.playing = true;
            gameValues.gamePiece = null;
            gameValues.nextgamePiece = null;
            gameValues.gameBoard = null;
            gameValues.speed = gameValues.getLevel();
            gameValues.points = 0;
            gameValues.gameTime = 0;
            lastMove = 0;
            gameValues.gameBoard = new boolean[x][y][z];
        }
        if (gameValues.gameTime == 0)
            gameValues.gameTime = System.currentTimeMillis();

        if (gameValues.nextgamePiece == null) {
            newPiece();
        } else if (gameValues.gamePiece == null) {
            nextgamePiece();
        }
    }

    protected void soundinit(Context _context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool();
        } else {
            createOldSoundPool();
        }
        sound[0] = sp.load(_context, R.raw.place, 1);
        sound[1] = sp.load(_context, R.raw.levelup, 1);
        sound[2] = sp.load(_context, R.raw.delete, 1);
        sound[3] = sp.load(_context, R.raw.delete2, 1);
        sound[4] = sp.load(_context, R.raw.empty, 1);
    }

    protected void createOldSoundPool() {
        sp = new SoundPool(2, AudioManager.USE_DEFAULT_STREAM_TYPE, 0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void createNewSoundPool() {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        sp = new SoundPool.Builder().setAudioAttributes(attributes).setMaxStreams(2).build();
    }

    public void draw(Canvas canvas) {
        if (isPlaying()) {

                basePaint.setColor(Color.WHITE);
                basePaint.setStyle(Style.STROKE);
                basePaint.setStrokeWidth(1);
                canvas.drawColor(Color.BLACK);
                basePaint.setStyle(Style.FILL);
                int max = -1;
                // recField = new Rect(height / 12, 0, 13 * height / 12, height);
                String recString = recField.flattenToString();
                planes(canvas);
                display(canvas);
                displaynextgamePiece(canvas);
                // Rect recFsmall = Rect.unflattenFromString(recField);
                for (int i = 0; i < 4; i++) {
                    recCornersBig[i] = Integer.parseInt(recString.split(" ")[i]);
                }
                // canvas.drawRect(Rect.unflattenFromString(recField), basePaint);
                int length = Math
                        .max(((Math.abs(recCornersBig[2] - recCornersBig[0])) / (gameValues.gameBoard[0].length))
                                        * (gameValues.gameBoard[0].length),
                                ((recCornersBig[3] - recCornersBig[1]) / (gameValues.gameBoard[0].length))
                                        * (gameValues.gameBoard[0].length));
                //recCornersBig[0] = gamePiecePosX(length);
                recCornersBig[1] = gamePiecePosY(length);
                recCornersBig[3] = recCornersBig[1]+length;
                recCornersBig[2] = recCornersBig[0] + length;
                Rect recFsmall = new Rect(recCornersBig[0], recCornersBig[1],
                        recCornersBig[2], recCornersBig[3]);
                float centerX = recFsmall.centerX();
                float centerY = recFsmall.centerY();

                int dx = (Math.abs(recCornersBig[2] - recCornersBig[0]))
                        / gameValues.gameBoard[0].length;
                for (int i = 0; i < (gameValues.gameBoard[0].length); i++) {
                    canvas.drawLine(recCornersBig[0] + i * dx,
                            recCornersBig[1], centerX, centerY, basePaint);
                    canvas.drawLine(recCornersBig[0] + i * dx,
                            recCornersBig[3], centerX, centerY, basePaint);
                    canvas.drawLine(recCornersBig[0], recCornersBig[1] + i
                            * dx, centerX, centerY, basePaint);
                    canvas.drawLine(recCornersBig[2], recCornersBig[1] + i
                            * dx, centerX, centerY, basePaint);
                }
                canvas.drawLine(recCornersBig[2], recCornersBig[3],
                        recFsmall.centerX(), recFsmall.centerY(), basePaint);
                dx = ((recFsmall.width() / 3) / gameValues.gameBoard[0][0].length);

                float p = recFsmall.width() / dx;
                canvas.drawRect(recFsmall, basePaint);
                for (int i = 0; i < gameValues.gameBoard[0][0].length; i++) {
                    dx = (int) (recFsmall.width() - Math
                            .ceil((recFsmall.width() - 2 * (recFsmall.width() / p))
                                    / gameValues.gameBoard.length)
                            * gameValues.gameBoard.length);
                    // dx = (int) (recFsmall.width() / p);
                    q[i] = dx;
                    // Log.i("p", " " + p + " dx " + dx);
                    recFsmall.inset(dx, dx);
                    canvas.drawRect(recFsmall, basePaint);
                }
                basePaint.setStyle(Style.FILL);
                basePaint.setColor(Color.BLACK);
                canvas.drawRect(recFsmall, basePaint);
                basePaint.setColor(Color.WHITE);
                basePaint.setStyle(Style.STROKE);
                for (int i = 0; i < 4; i++) {
                    recCornersSmall[i] = Integer.parseInt(recFsmall
                            .flattenToString().split(" ")[i]);
                }
                float dxx = (recCornersSmall[2] - recCornersSmall[0])
                        / gameValues.gameBoard[0].length;
                for (int i = 1; i < (gameValues.gameBoard[0].length); i++) {
                    canvas.drawLine((recCornersSmall[0] + i * dxx),
                            recCornersSmall[1], (recCornersSmall[0] + i * dxx),
                            recCornersSmall[3], basePaint);
                    canvas.drawLine(recCornersSmall[0], (recCornersSmall[1] + i
                            * dxx), recCornersSmall[2], (recCornersSmall[1] + i
                            * dxx), basePaint);
                }
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    for (int k = 0; k < gameValues.gameBoard[0][0].length; k++) {
                        if (gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1]][k])
                            max = Math.max(max, k);
                    }
                }
                for (int i = 0; i < (gameValues.gameBoard.length); i++) {
                    for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                        boolean shadow = false;
                        for (int k = 0; k < gameValues.gamePiece.length; k++) {
                            if ((gameValues.gamePiece[k][0] == i)
                                    && (gameValues.gamePiece[k][1] == j)) {
                                shadow = true;
                            }
                        }
                        if (shadow) {
                            basePaint.setStrokeWidth(1);
                            basePaint.setStyle(Style.FILL);
                            basePaint.setColor(Color.GRAY);
                            basePaint.setAlpha(155);
                            int scale = 0;
                            for (int s = 0; s < (9 - max); s++) {
                                scale = scale + q[s];
                            }
                            float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
                            float gleft = recCornersBig[0] + scale + i * size;
                            float gtop = recCornersBig[1] + scale + (j * size);
                            float gright = gleft + size;
                            float gbottom = gtop + size;
                            canvas.drawRect(gleft, gtop, gright, gbottom, basePaint);
                            basePaint.setStyle(Style.STROKE);
                            basePaint.setColor(Color.WHITE);
                            basePaint.setAlpha(200);
                            canvas.drawRect(gleft, gtop, gright, gbottom, basePaint);
                        }
                    }
                }

                for (int k = 0; k < gameValues.gameBoard[0][0].length; k++) {
                    for (int i = 0; i < (gameValues.gameBoard.length / 2 + gameValues.gameBoard.length % 2); i++) {
                        for (int j = 0; j < gameValues.gameBoard[0].length / 2
                                + gameValues.gameBoard.length % 2; j++) {

                            gameBoardDisplay(canvas, i, j, k);
                            int ii = gameValues.gameBoard.length - 1 - i;
                            int jj = gameValues.gameBoard.length - 1 - j;
                            if (ii > i) {
                                gameBoardDisplay(canvas, ii, j, k);
                            }
                            if (jj > j) {
                                gameBoardDisplay(canvas, i, jj, k);
                            }
                            if ((ii > i) && (jj > j)) {
                                gameBoardDisplay(canvas, ii, jj, k);
                            }
                        }
                    }
                }
                frame(canvas);
                drawGamePiece(canvas);
                //debugButtons(canvas);

        } else {
            basePaint.setStyle(Style.FILL);
            basePaint.setColor(Color.BLACK);
            if (basePaint.getAlpha() == 255)
                basePaint.setAlpha(100);
            canvas.drawRect(0, 0, width, height, basePaint);
            basePaint.setColor(Color.WHITE);
            basePaint.setTextSize(canvas.getHeight() / 10);
            basePaint.setTextAlign(Align.CENTER);
            canvas.drawText("Game Over", width / 2, height / 2, basePaint);
        }
    }

    private int gamePiecePosY(int length) {
        int pos=0;
        int down = 0;
        int size=length/gameValues.gameBoard.length;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            down = Math.max(down, gameValues.gamePiece[i][1]);
        }
        int up = gameValues.gameBoard.length;
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                up = Math.min(up, gameValues.gamePiece[i][1]);
            }
        if ((up+1)*size<length-height) {
            pos = 0;
        }
        else if ((down)*size>height) {
            pos = height - length;
        }
        else {
            pos =height/2-length/2;
        }
        return pos;
    }

    private void gameBoardDisplay(Canvas canvas, int i, int j, int k) {
        if (gameValues.gameBoard[i][j][k]) {
            int scale = 0;
            for (int s = 0; s < (9 - k); s++) {
                scale = scale + q[s];
            }
            int size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
            float left = recCornersBig[0] + scale + i * size;
            float top = recCornersBig[1] + scale + (j * size);
            float right = left + size;
            float bottom = top + size;
            scale = 0;
            for (int s = 0; s < (10 - k); s++) {
                scale = scale + q[s];
            }
            size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
            float bleft = recCornersBig[0] + scale + i * size;
            float btop = recCornersBig[1] + scale + (j * size);
            float bright = bleft + size;
            float bbottom = btop + size;
            basePaint.setStyle(Style.STROKE);
            basePaint.setColor(Color.WHITE);
            basePaint.setStrokeWidth(1);
            Path rhombus = new Path();
            if (j > 0) {
                if (!gameValues.gameBoard[i][j - 1][k]) {
                    rhombus.moveTo(left, top);
                    rhombus.lineTo(right, top);
                    rhombus.lineTo(bright, btop);
                    rhombus.lineTo(bleft, btop);
                    rhombus.close();
                    rhombus.setFillType(FillType.WINDING);
                    basePaint.setStyle(Style.FILL);
                    basePaint.setColor(bgcolor[k]);
                    basePaint.setAlpha(150);
                    canvas.drawPath(rhombus, basePaint);
                    basePaint.setStyle(Style.STROKE);
                    basePaint.setColor(Color.WHITE);

                    canvas.drawLine(left, top, bleft, btop, basePaint);
                    canvas.drawLine(right, top, bright, btop, basePaint);
                    canvas.drawLine(bleft, btop, bright, btop, basePaint);

                }
            }
            if (i < gameValues.gameBoard.length - 1) {
                if (!gameValues.gameBoard[i + 1][j][k]) {
                    rhombus = new Path();
                    rhombus.moveTo(right, top);
                    rhombus.lineTo(bright, btop);
                    rhombus.lineTo(bright, bbottom);
                    rhombus.lineTo(right, bottom);
                    rhombus.close();
                    rhombus.setFillType(FillType.WINDING);
                    basePaint.setStyle(Style.FILL);
                    basePaint.setColor(bgcolor[k]);
                    basePaint.setAlpha(150);
                    canvas.drawPath(rhombus, basePaint);
                    basePaint.setStyle(Style.STROKE);
                    basePaint.setColor(Color.WHITE);
                    canvas.drawLine(right, top, bright, btop, basePaint);
                    canvas.drawLine(right, bottom, bright, bbottom, basePaint);
                    canvas.drawLine(bright, btop, bright, bbottom, basePaint);

                }
            }
            if (j < gameValues.gameBoard.length - 1) {
                if (!gameValues.gameBoard[i][j + 1][k]) {
                    rhombus = new Path();
                    rhombus.moveTo(right, bottom);
                    rhombus.lineTo(left, bottom);
                    rhombus.lineTo(bleft, bbottom);
                    rhombus.lineTo(bright, bbottom);
                    rhombus.close();
                    rhombus.setFillType(FillType.WINDING);
                    basePaint.setStyle(Style.FILL);
                    basePaint.setColor(bgcolor[k]);
                    basePaint.setAlpha(150);
                    canvas.drawPath(rhombus, basePaint);
                    basePaint.setStyle(Style.STROKE);
                    basePaint.setColor(Color.WHITE);
                    canvas.drawLine(right, bottom, bright, bbottom, basePaint);
                    canvas.drawLine(left, bottom, bleft, bbottom, basePaint);
                    canvas.drawLine(bright, bbottom, bleft, bbottom, basePaint);

                }
            }
            if (i > 0) {
                if (!gameValues.gameBoard[i - 1][j][k]) {
                    rhombus = new Path();
                    rhombus.moveTo(left, top);
                    rhombus.lineTo(bleft, btop);
                    rhombus.lineTo(bleft, bbottom);
                    rhombus.lineTo(left, bottom);
                    rhombus.close();
                    rhombus.setFillType(FillType.WINDING);
                    basePaint.setStyle(Style.FILL);
                    basePaint.setColor(bgcolor[k]);
                    basePaint.setAlpha(150);
                    canvas.drawPath(rhombus, basePaint);
                    basePaint.setStyle(Style.STROKE);
                    basePaint.setColor(Color.WHITE);
                    canvas.drawLine(left, bottom, bleft, bbottom, basePaint);
                    canvas.drawLine(left, top, bleft, btop, basePaint);
                    canvas.drawLine(bleft, bbottom, bleft, btop, basePaint);

                }
            }

            boolean shadow = false;
            for (int l = 0; l < gameValues.gamePiece.length; l++) {
                if ((gameValues.gamePiece[l][0] == i)
                        && (gameValues.gamePiece[l][1] == j)) {
                    shadow = true;
                }
            }
            if (k + 1 < gameValues.gameBoard[0][0].length) {
                if (!gameValues.gameBoard[i][j][k + 1]) {
                    basePaint.setStyle(Style.FILL);

                    basePaint.setColor(bgcolor[k]);
                    basePaint.setAlpha(200);
                    canvas.drawRect(left, top, right, bottom, basePaint);
                    if (shadow) {
                        basePaint.setColor(Color.GRAY);
                        basePaint.setAlpha(150);
                        canvas.drawRect(left, top, right, bottom, basePaint);
                    }
                    basePaint.setStyle(Style.STROKE);
                    basePaint.setColor(Color.WHITE);
                }
            }
            basePaint.setColor(Color.WHITE);
            basePaint.setAlpha(200);
            canvas.drawRect(left, top, right, bottom, basePaint);
            basePaint.setColor(Color.WHITE);
        }
    }

    @SuppressLint("DefaultLocale")
    private void display(Canvas canvas) {
        if (lastMove == 0) {
            lastMove = gameValues.gameTime;
        } else {
            if (System.currentTimeMillis() - lastMove > 1000 * (11 - gameValues.speed)) {
                moveBack();
                lastMove = System.currentTimeMillis();
            }
        }
        int left = recCornersBig[2] + 4;
        int top = 2;
        int right = canvas.getWidth() - 3;
        int disHeight = canvas.getHeight() / 4;
        int disWidth = right - left;
        int ts = canvas.getHeight() / 35;
        String minutes = String
                .format("%02d",
                        (int) ((System.currentTimeMillis() - gameValues.gameTime) / 1000) / 60);
        String seconds = String
                .format("%02d",
                        (int) (((System.currentTimeMillis() - gameValues.gameTime) / 1000) % 60));
        basePaint.setTextAlign(Align.LEFT);
        basePaint.setStyle(Style.FILL);
        basePaint.setTextSize(ts);
        canvas.drawText("Stage:", left + 8, ts + top + 2, basePaint);
        canvas.drawText("Time:", left + disWidth / 2 + 8, ts + top + 2, basePaint);
        canvas.drawText("Level:", left + 8, ts + disHeight / 2 + 2, basePaint);
        canvas.drawText("Points:", left + disWidth / 2 + 8, ts + disHeight / 2 + 2, basePaint);
        canvas.drawText("Next Piece:", left + 8, ts + disHeight + 2, basePaint);
        int ts2 = disHeight / 5;
        basePaint.setTextSize(ts2);
        basePaint.setTextAlign(Align.RIGHT);
        canvas.drawText(gameValues.gameBoard.length + "x"
                        + gameValues.gameBoard.length + "x"
                        + gameValues.gameBoard[0][0].length, left + disWidth / 2 - ts2,
                ts + ts2, basePaint);
        canvas.drawText(gameValues.speed + "", left + disWidth / 2 - ts2, ts2 + ts + disHeight / 2 + 2, basePaint);
        basePaint.setTextAlign(Align.RIGHT);
        canvas.drawText(minutes + ":" + seconds, left + 15 * disWidth / 16, ts + ts2, basePaint);
        canvas.drawText(gameValues.points + "", left + 15 * disWidth / 16, ts2 + ts + disHeight / 2 + 2, basePaint);
        canvas.drawBitmap(help, null, controlFrame, basePaint);
        int scenter = (disHeight + height - width + recCornersBig[2]) / 2;
        soundRect = new Rect(right - disWidth / 5 - 2, scenter - disWidth / 5, right - 2, scenter);
        if (gameValues.sound) {
            canvas.drawBitmap(soundon, null, soundRect, basePaint);
        } else {
            canvas.drawBitmap(soundoff, null, soundRect, basePaint);
        }
        basePaint.setStyle(Style.STROKE);
    }

    private void displaynextgamePiece(Canvas canvas) {
        int minx = gameValues.gameBoard.length;
        int miny = gameValues.gameBoard.length;
        int maxx = 0;
        int maxy = 0;
        int left = (recCornersBig[2] + width) / 2;
        int bottom = height - width + recCornersBig[2];
        int top = (height / 4 + bottom) / 2;
        int size = (bottom - top) / 2;
        if (!(gameValues.nextgamePiece == null)) {
            basePaint.setStrokeWidth(2);
            // Rect rect = new Rect(left, top, right, bottom);
            // canvas.drawRect(rect, basePaint);
            for (int i = 0; i < gameValues.nextgamePiece.length; i++) {
                minx = Math.min(minx, gameValues.nextgamePiece[i][0]);
                miny = Math.min(miny, gameValues.nextgamePiece[i][1]);
                maxx = Math.max(maxx, gameValues.nextgamePiece[i][0]);
                maxy = Math.max(maxy, gameValues.nextgamePiece[i][1]);

            }
            left = left - (maxx - minx + 1) * (size) / 2;
            top = top - (maxy - miny + 1) * (size) / 2;
            for (int i = 0; i < gameValues.nextgamePiece.length; i++) {
                int dx = gameValues.nextgamePiece[i][0] - minx;
                int dy = gameValues.nextgamePiece[i][1] - miny;
                canvas.drawRect(left + dx * size, top + dy * size, left
                        + (dx + 1) * size, top + (dy + 1) * size, basePaint);

            }
            basePaint.setStrokeWidth(1);
        }
    }

    private void planes(Canvas canvas) {
        int max = -1;
        basePaint.setColor(Color.BLACK);
        canvas.drawRect(planesRect, basePaint);
        for (int k = 0; k < gameValues.gameBoard[0][0].length; k++) {
            for (int i = 0; i < gameValues.gameBoard.length; i++) {
                for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                    if ((gameValues.gameBoard[i][j][k]) && (k > max)) {
                        max = k;
                    }
                }
            }
        }
        int size = canvas.getHeight() / 10;
        for (int i = 0; i <= max; i++) {
            basePaint.setStyle(Style.FILL);
            basePaint.setColor(bgcolor[i]);
            canvas.drawRect(6, canvas.getHeight() - (i + 1) * size - 2,
                    canvas.getHeight() / 12 - 6, canvas.getHeight() - (i)
                            * size - 4, basePaint);
        }
        basePaint.setStyle(Style.STROKE);
        basePaint.setColor(Color.WHITE);
    }

    private void drawGamePiece(Canvas canvas) {
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int cx = gameValues.gamePiece[i][0];
            int cy = gameValues.gamePiece[i][1];
            int cz = gameValues.gamePiece[i][2];

            // Log.i("gamePiece", cx + "x" + cy + "x" + cz);
            int scale = 0;
            for (int s = 0; s < (9 - cz); s++) {
                scale = scale + q[s];
            }
            float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
            float left = recCornersBig[0] + scale + cx * size;
            float top = recCornersBig[1] + scale + (cy * size);
            float right = left + size;
            float bottom = top + size;
            scale = 0;
            for (int s = 0; s < (10 - cz); s++) {
                scale = scale + q[s];
            }
            size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
            float bleft = recCornersBig[0] + scale + cx * size;
            float btop = recCornersBig[1] + scale + (cy * size);
            float bright = bleft + size;
            float bbottom = btop + size;
            basePaint.setStrokeWidth(2);
            canvas.drawLine(left, top, left, bottom, basePaint);
            canvas.drawLine(bleft, btop, bleft, bbottom, basePaint);
            canvas.drawLine(left, top, right, top, basePaint);
            canvas.drawLine(bleft, btop, bright, btop, basePaint);
            canvas.drawLine(right, top, right, bottom, basePaint);
            canvas.drawLine(bright, btop, bright, bbottom, basePaint);
            canvas.drawLine(left, bottom, right, bottom, basePaint);
            canvas.drawLine(bleft, bbottom, bright, bbottom, basePaint);
            canvas.drawLine(left, top, bleft, btop, basePaint);
            canvas.drawLine(right, top, bright, btop, basePaint);
            canvas.drawLine(left, bottom, bleft, bbottom, basePaint);
            canvas.drawLine(right, bottom, bright, bbottom, basePaint);
            basePaint.setStrokeWidth(1);
        }
    }

    private void moveRight() {
        int right = 0;
        boolean moveable = true;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            right = Math.max(right, gameValues.gamePiece[i][0]);
        }
        if (right < gameValues.gameBoard.length - 1) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                if (gameValues.gameBoard[gameValues.gamePiece[i][0] + 1][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2]])
                    moveable = false;
            }
            if (moveable) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    gameValues.gamePiece[i][0]++;
                }
            }
        }
    }

    private void moveLeft() {
        boolean moveable = true;
        int left = gameValues.gameBoard.length;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            left = Math.min(left, gameValues.gamePiece[i][0]);
        }
        if (left > 0) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                if (gameValues.gameBoard[gameValues.gamePiece[i][0] - 1][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2]])
                    moveable = false;
            }
            if (moveable) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    gameValues.gamePiece[i][0]--;
                }
            }
        }
    }

    private void moveDown() {
        boolean moveable = true;
        int down = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            down = Math.max(down, gameValues.gamePiece[i][1]);
        }
        if (down < gameValues.gameBoard.length - 1) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                if (gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1] + 1][gameValues.gamePiece[i][2]])
                    moveable = false;
            }
            if (moveable) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    gameValues.gamePiece[i][1]++;
                }
            }
        }
    }

    private void moveUp() {
        boolean moveable = true;
        int up = gameValues.gameBoard.length;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            up = Math.min(up, gameValues.gamePiece[i][1]);
        }
        if (up > 0) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                if (gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1] - 1][gameValues.gamePiece[i][2]])
                    moveable = false;
            }
            if (moveable) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    gameValues.gamePiece[i][1]--;
                }
            }
        }
    }

    public void moveFront() {
        int back = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            back = Math.max(back, gameValues.gamePiece[i][2]);
        }
        if (back < gameValues.gameBoard[0][0].length - 1) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                gameValues.gamePiece[i][2]++;
            }
        }
    }

    private void moveBack() {
        boolean moveable = true;
        int front = gameValues.gameBoard[0][0].length;
        int back = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            front = Math.min(front, gameValues.gamePiece[i][2]);
            back = Math.max(back, gameValues.gamePiece[i][2]);
        }
        if (front > 0) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                if (gameValues.gamePiece[i][2] < gameValues.gameBoard[0][0].length) {
                    if (gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2] - 1]) {
                        moveable = false;
                    }
                }
            }

            if (moveable) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    gameValues.gamePiece[i][2]--;
                }
            } else {
                storePiece();
            }
        } else {
            storePiece();
        }
    }

    private void turnYccw() {
        boolean moveable = true;
        int medx = median(0);
        int medz = median(2);
        int xadj = 0;
        int zadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx + (gameValues.gamePiece[i][2] - medz);
            int tz = medz - (gameValues.gamePiece[i][0] - medx);

            if (tx < 0)
                xadj = Math.max(xadj, -tx);
            if (tz < 0)
                zadj = Math.max(zadj, -tz);
            if (tx >= gameValues.gameBoard.length)
                xadj = Math.min(xadj, gameValues.gameBoard.length - 1 - tx);
            if (tz >= gameValues.gameBoard[0][0].length)
                zadj = Math.min(zadj, gameValues.gameBoard[0][0].length - 1
                        - tz);
        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx + (gameValues.gamePiece[i][2] - medz) + xadj;
            int tz = medz - (gameValues.gamePiece[i][0] - medx) + zadj;
            if (gameValues.gameBoard[tx][gameValues.gamePiece[i][1]][tz]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int tx = medx + (gameValues.gamePiece[i][2] - medz) + xadj;
                int tz = medz - (gameValues.gamePiece[i][0] - medx) + zadj;
                gameValues.gamePiece[i][0] = tx;
                gameValues.gamePiece[i][2] = tz;
            }
        }
    }

    private void turnYcw() {
        boolean moveable = true;
        int medx = median(0);
        int medz = median(2);
        int xadj = 0;
        int zadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx - (gameValues.gamePiece[i][2] - medz);
            int tz = medz + (gameValues.gamePiece[i][0] - medx);

            if (tx < 0)
                xadj = Math.max(xadj, -tx);
            if (tz < 0)
                zadj = Math.max(zadj, -tz);
            if (tx >= gameValues.gameBoard.length)
                xadj = Math.min(xadj, gameValues.gameBoard.length - 1 - tx);
            if (tz >= gameValues.gameBoard[0][0].length)
                zadj = Math.min(zadj, gameValues.gameBoard[0][0].length - 1
                        - tz);
        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx - (gameValues.gamePiece[i][2] - medz) + xadj;
            int tz = medz + (gameValues.gamePiece[i][0] - medx) + zadj;
            if (gameValues.gameBoard[tx][gameValues.gamePiece[i][1]][tz]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int tx = medx - (gameValues.gamePiece[i][2] - medz) + xadj;
                int tz = medz + (gameValues.gamePiece[i][0] - medx) + zadj;
                gameValues.gamePiece[i][0] = tx;
                gameValues.gamePiece[i][2] = tz;
            }
        }
    }

    private void turnXcw() {
        boolean moveable = true;
        int medy = median(1);
        int medz = median(2);
        int yadj = 0;
        int zadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int ty = medy + (gameValues.gamePiece[i][2] - medz);
            int tz = medz - (gameValues.gamePiece[i][1] - medy);
            if (ty < 0)
                yadj = Math.max(yadj, -ty);
            if (tz < 0)
                zadj = Math.max(zadj, -tz);
            if (ty >= gameValues.gameBoard.length)
                yadj = Math.min(yadj, gameValues.gameBoard.length - 1 - ty);
            if (tz >= gameValues.gameBoard[0][0].length)
                zadj = Math.min(zadj, gameValues.gameBoard[0][0].length - 1
                        - tz);
        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int ty = medy + (gameValues.gamePiece[i][2] - medz) + yadj;
            int tz = medz - (gameValues.gamePiece[i][1] - medy) + zadj;
            if (gameValues.gameBoard[gameValues.gamePiece[i][0]][ty][tz]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int ty = medy + (gameValues.gamePiece[i][2] - medz) + yadj;
                int tz = medz - (gameValues.gamePiece[i][1] - medy) + zadj;
                gameValues.gamePiece[i][1] = ty;
                gameValues.gamePiece[i][2] = tz;
            }
        }
    }

    private void turnXccw() {
        boolean moveable = true;
        int medy = median(1);
        int medz = median(2);
        int yadj = 0;
        int zadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int ty = medy - (gameValues.gamePiece[i][2] - medz);
            int tz = medz + (gameValues.gamePiece[i][1] - medy);
            if (ty < 0)
                yadj = Math.max(yadj, -ty);
            if (tz < 0)
                zadj = Math.max(zadj, -tz);
            if (ty >= gameValues.gameBoard.length)
                yadj = Math.min(yadj, gameValues.gameBoard.length - 1 - ty);
            if (tz >= gameValues.gameBoard[0][0].length)
                zadj = Math.min(zadj, gameValues.gameBoard[0][0].length - 1
                        - tz);
        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int ty = medy - (gameValues.gamePiece[i][2] - medz) + yadj;
            int tz = medz + (gameValues.gamePiece[i][1] - medy) + zadj;
            if (gameValues.gameBoard[gameValues.gamePiece[i][0]][ty][tz]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int ty = medy - (gameValues.gamePiece[i][2] - medz) + yadj;
                int tz = medz + (gameValues.gamePiece[i][1] - medy) + zadj;
                gameValues.gamePiece[i][1] = ty;
                gameValues.gamePiece[i][2] = tz;
            }
        }
    }

    private void turnZccw() {
        boolean moveable = true;
        int medx = median(0);
        int medy = median(1);
        int xadj = 0;
        int yadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx + (gameValues.gamePiece[i][1] - medy);
            int ty = medy - (gameValues.gamePiece[i][0] - medx);
            if (tx < 0)
                xadj = Math.max(xadj, -tx);
            if (tx >= gameValues.gameBoard.length)
                xadj = Math.min(xadj, gameValues.gameBoard.length - 1 - tx);
            if (ty < 0)
                yadj = Math.max(yadj, -ty);
            if (ty >= gameValues.gameBoard.length)
                yadj = Math.min(yadj, gameValues.gameBoard.length - 1 - ty);

        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx + (gameValues.gamePiece[i][1] - medy) + xadj;
            int ty = medy - (gameValues.gamePiece[i][0] - medx) + yadj;
            if (gameValues.gameBoard[tx][ty][gameValues.gamePiece[i][2]]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int tx = medx + (gameValues.gamePiece[i][1] - medy) + xadj;
                int ty = medy - (gameValues.gamePiece[i][0] - medx) + yadj;
                gameValues.gamePiece[i][0] = tx;
                gameValues.gamePiece[i][1] = ty;
            }
        }
    }

    private void turnZcw() {
        boolean moveable = true;
        int medx = median(0);
        int medy = median(1);
        int xadj = 0;
        int yadj = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx - (gameValues.gamePiece[i][1] - medy);
            int ty = medy + (gameValues.gamePiece[i][0] - medx);
            if (tx < 0)
                xadj = Math.max(xadj, -tx);
            if (tx >= gameValues.gameBoard.length)
                xadj = Math.min(xadj, gameValues.gameBoard.length - 1 - tx);
            if (ty < 0)
                yadj = Math.max(yadj, -ty);
            if (ty >= gameValues.gameBoard.length)
                yadj = Math.min(yadj, gameValues.gameBoard.length - 1 - ty);

        }
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            int tx = medx - (gameValues.gamePiece[i][1] - medy) + xadj;
            int ty = medy + (gameValues.gamePiece[i][0] - medx) + yadj;
            if (gameValues.gameBoard[tx][ty][gameValues.gamePiece[i][2]]) {
                moveable = false;
            }
        }
        if (moveable) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                int tx = medx - (gameValues.gamePiece[i][1] - medy) + xadj;
                int ty = medy + (gameValues.gamePiece[i][0] - medx) + yadj;
                gameValues.gamePiece[i][0] = tx;
                gameValues.gamePiece[i][1] = ty;
            }
        }
    }

    private int median(int xyz) {
        float med = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            med += gameValues.gamePiece[i][xyz];
        }
        med = Math.round(med / gameValues.gamePiece.length);
        return (int) med;
    }

    public int max(int xyz) {
        int max = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            max = Math.max(max, gameValues.gamePiece[i][xyz]);
        }
        return max;
    }

    public int min(int xyz) {
        int min = gameValues.gameBoard.length;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            min = Math.min(min, gameValues.gamePiece[i][xyz]);
        }
        return min;
    }

    private void toGround() {
        int front = gameValues.gameBoard[0][0].length + 1;
        boolean cont = true;
        while (cont) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                front = Math.min(front, gameValues.gamePiece[i][2]);
            }
            if (front > 0) {
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    if (gameValues.gamePiece[i][2] < gameValues.gameBoard[0][0].length) {
                        if (gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2] - 1]) {
                            cont = false;
                        }
                    }
                }
                if (cont) {
                    for (int i = 0; i < gameValues.gamePiece.length; i++) {
                        gameValues.gamePiece[i][2]--;
                    }
                }
            } else {
                cont = false;
            }
        }
        if (!cont) {
            storePiece();
        }
    }

    private void storePiece() {
        boolean empty = true;
        int max = 0;
        int fullRows = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            max = Math.max(max, gameValues.gamePiece[i][2]);
        }
        if (max < gameValues.gameBoard[0][0].length - 1) {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2]] = true;
            }
            for (int k = 0; k < gameValues.gameBoard[0][0].length; k++) {
                boolean full = true;
                for (int i = 0; i < gameValues.gameBoard.length; i++) {
                    for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                        if (!gameValues.gameBoard[i][j][k])
                            full = false;
                    }
                }
                if (full) {
                    fullRows++;
                    for (int i = 0; i < gameValues.gameBoard.length; i++) {
                        for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                            gameValues.gameBoard[i][j][k] = false;
                        }
                    }
                }
            }
            int filledPlanes = 0;
            for (int l = 0; l < 4; l++) {
                for (int k = 0; k < gameValues.gameBoard[0][0].length - 1; k++) {
                    empty = true;
                    for (int i = 0; i < gameValues.gameBoard.length; i++) {
                        for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                            if (gameValues.gameBoard[i][j][k])
                                empty = false;
                            filledPlanes++;
                        }
                    }
                    if (empty) {
                        for (int i = 0; i < gameValues.gameBoard.length; i++) {
                            for (int j = 0; j < gameValues.gameBoard[0].length; j++) {
                                gameValues.gameBoard[i][j][k] = gameValues.gameBoard[i][j][k + 1];
                                gameValues.gameBoard[i][j][k + 1] = false;
                            }
                        }
                    }
                }
            }

            addPoints(Math.pow(fullRows, 2) / 2 * gameValues.gameBoard.length
                    * gameValues.gameBoard[0].length);
            addPoints(gameValues.gamePiece.length);
            if ((filledPlanes == 0) && (gameValues.points > 0)) {
                playSound(4);
            } else {
                if (fullRows > 1) {
                    playSound(3);
                } else if (fullRows == 1) {
                    playSound(2);
                } else {
                    playSound(0);
                }
            }
            nextgamePiece();
        } else {
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                gameValues.gameBoard[gameValues.gamePiece[i][0]][gameValues.gamePiece[i][1]][gameValues.gamePiece[i][2]] = true;
            }
            gameOver();
        }
    }

    private void playSound(int wav) {
        if (gameValues.sound)
            sp.play(sound[wav], .2f, .2f, 0, 0, 1f);
    }

    private void gameOver() {
        gameValues.updateScores();
        gameValues.playing = false;
    }

    private boolean isPlaying() {
        return gameValues.playing;
    }

    private void addPoints(double d) {
        gameValues.points += 2 * d * gameValues.speed;
        if ((gameValues.speed < 10)
                && (Math.pow(2, gameValues.speed) / 2 < (gameValues.points / 1000) + 1)) {
            gameValues.speed++;
            gameValues.levelup++;
            playSound(1);
        }
    }

    private void newPiece() {
        lastMove = System.currentTimeMillis();
        gameValues.gamePiece = null;
        gameValues.nextgamePiece = null;
        Random rand = new Random();
        int r = rand.nextInt(GameValues.Pieces.length);
        int rnext = rand.nextInt(GameValues.Pieces.length);
        if (r == rnext) {
            rnext = rand.nextInt(GameValues.Pieces.length);
        }
        gameValues.gamePiece = new int[GameValues.Pieces[r].length][3];
        gameValues.nextgamePiece = new int[GameValues.Pieces[rnext].length][3];
        for (int i = 0; i < GameValues.Pieces[r].length; i++) {
            System.arraycopy(GameValues.Pieces[r][i], 0, gameValues.gamePiece[i], 0, GameValues.Pieces[r][i].length);
        }
        for (int i = 0; i < GameValues.Pieces[rnext].length; i++) {
            System.arraycopy(GameValues.Pieces[rnext][i], 0, gameValues.nextgamePiece[i], 0, GameValues.Pieces[rnext][i].length);
        }
    }

    private void nextgamePiece() {
        lastMove = System.currentTimeMillis();
        Random rand = new Random();
        gameValues.gamePiece = null;
        gameValues.gamePiece = new int[gameValues.nextgamePiece.length][3];
        for (int i = 0; i < gameValues.nextgamePiece.length; i++) {
            System.arraycopy(gameValues.nextgamePiece[i], 0, gameValues.gamePiece[i], 0, gameValues.nextgamePiece[i].length);
        }
        int rnext = rand.nextInt(GameValues.Pieces.length);
        gameValues.nextgamePiece = null;
        gameValues.nextgamePiece = new int[GameValues.Pieces[rnext].length][3];
        for (int i = 0; i < GameValues.Pieces[rnext].length; i++) {
            System.arraycopy(GameValues.Pieces[rnext][i], 0, gameValues.nextgamePiece[i], 0, GameValues.Pieces[rnext][i].length);
        }
    }

    private int position(float x2, float y2, int xmin, int xmax, int ymin,
                         int ymax) {
        int pos = 0;
        if ((x2 < xmin) && (y2 > ymin) && (y2 < ymax))
            pos = 1; // left
        else if ((x2 > xmax) && (y2 > ymin) && (y2 < ymax))
            pos = 2; // right
        else if ((y2 < ymin) && (x2 > xmin) && (x2 < xmax))
            pos = 4; // top
        else if ((y2 > ymax) && (x2 > xmin) && (x2 < xmax))
            pos = 5; // bottom
        else if ((x2 > xmin) && (x2 < xmax) && (y2 > ymin) && (y2 < ymax))
            pos = 3; // inside
        return pos;
    }

    public void buttonRelease(float xx, float yy) {
        if (isPlaying()) {
            if (!moved) {
                float dx = touchx - xx;
                float dy = touchy - yy;
                int xmin = gameValues.gameBoard.length, ymin = gameValues.gameBoard.length, xmax = 0, ymax = 0;
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    xmin = Math.min(xmin, gameValues.gamePiece[i][0]);
                    ymin = Math.min(ymin, gameValues.gamePiece[i][1]);
                    xmax = Math.max(xmax, gameValues.gamePiece[i][0]);
                    ymax = Math.max(ymax, gameValues.gamePiece[i][1]);

                }
                int scale = 0;
                for (int s = 0; s < (9 - median(2)); s++) {
                    scale = scale + q[s];
                }
                float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
                xmin = (int) (recCornersBig[0] + scale + xmin * size - size / 3);
                xmax = (int) (recCornersBig[0] + scale + (xmax + 1) * size + size / 3);
                ymin = (int) (recCornersBig[1] + scale + ymin * size - size / 3);
                ymax = (int) (recCornersBig[1] + scale + (ymax + 1) * size + size / 3);
                size=Math.round(size*0.7);
                if (Math.abs(dx) >= size || Math.abs(dy) >= size) {
                    if (Math.abs(dx) > Math.abs(dy)) {
                        if (dx > 0) { // left
                            switch (position(touchx, touchy, xmin, xmax, ymin, ymax)) {
                                case 1: // left
                                    // moveLeft();
                                    break;
                                case 2: // right
                                    turnYcw();
                                    break;
                                case 3: // inside
                                    // moveLeft();
                                    break;
                                case 4: // top
                                    turnZccw();
                                    break;
                                case 5: // bottom
                                    turnZcw();
                                    break;
                            }
                        } else { // right
                            switch (position(touchx, touchy, xmin, xmax, ymin, ymax)) {
                                case 1:
                                    turnYccw();
                                    break;
                                case 2:
                                    // moveRight();
                                    break;
                                case 3:
                                    // moveRight();
                                    break;
                                case 4:
                                    turnZcw();
                                    break;
                                case 5:
                                    turnZccw();
                                    break;
                            }
                        }
                    } else {
                        if (dy > 0) { // up
                            switch (position(touchx, touchy, xmin, xmax, ymin, ymax)) {
                                case 1:
                                    turnZcw();
                                    break;
                                case 2:
                                    turnZccw();
                                    break;
                                case 3:
                                    // moveUp();
                                    break;
                                case 4:
                                    // moveUp();
                                    break;
                                case 5:
                                    turnXccw();
                                    break;
                            }

                        } else { // down
                            switch (position(touchx, touchy, xmin, xmax, ymin, ymax)) {
                                case 1:
                                    turnZccw();
                                    break;
                                case 2:
                                    turnZcw();
                                    break;
                                case 3:
                                    // moveDown();
                                    break;
                                case 4:
                                    turnXcw();
                                    break;
                                case 5:
                                    // moveDown();
                                    break;
                            }

                        }
                    }
                    grounded = false;
                } else {
                    if ((System.currentTimeMillis() - lasttouch < 500)
                            && (position(xx, yy, xmin, xmax, ymin, ymax) == 3)
                            && (!grounded)) {
                        toGround();
                        grounded = true;
                    } else {
                        grounded = false;
                    }
                }
                lasttouch = System.currentTimeMillis();
            }
        }

    }

    public void buttonMove(float x2, float y2) {
        if (isPlaying()) {

            int xmin = gameValues.gameBoard.length, ymin = gameValues.gameBoard.length, xmax = 0, ymax = 0;
            for (int i = 0; i < gameValues.gamePiece.length; i++) {
                xmin = Math.min(xmin, gameValues.gamePiece[i][0]);
                ymin = Math.min(ymin, gameValues.gamePiece[i][1]);
                xmax = Math.max(xmax, gameValues.gamePiece[i][0]);
                ymax = Math.max(ymax, gameValues.gamePiece[i][1]);

            }
            int scale = 0;
            for (int s = 0; s < (9 - median(2)); s++) {
                scale = scale + q[s];
            }
            float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
            xmin = (int) (recCornersBig[0] + scale + xmin * size);
            xmax = (int) (recCornersBig[0] + scale + (xmax + 1) * size);
            ymin = (int) (recCornersBig[1] + scale + ymin * size);
            ymax = (int) (recCornersBig[1] + scale + (ymax + 1) * size);
            size=Math.round(size*0.7);
            if (position(touchx, touchy, xmin, xmax, ymin, ymax) == 3) {
                if (touchx - x2 >= size) {
                    moved = true;
                    moveLeft();
                    touchx = x2;
                    touchy = y2;
                }
                if (x2 - touchx >= size) {
                    moved = true;
                    moveRight();
                    touchx = x2;
                    touchy = y2;
                }
                if (touchy - y2 >= size) {
                    moved = true;
                    moveUp();
                    touchx = x2;
                    touchy = y2;
                }
                if (y2 - touchy >= size) {
                    moved = true;
                    moveDown();
                    touchx = x2;
                    touchy = y2;
                }

            }
            // helpPosition=position(x2, y2, xmin, xmax, ymin, ymax);
        }
    }

    public void buttonPressed(float xx, float yy) {
        if (isPlaying()) {
            if (soundRect.contains((int) xx, (int) yy)) {
                gameValues.sound = !gameValues.sound;
            } else if (controlFrame.contains((int) xx, (int) yy)) {
                xx = xx - controlFrame.left;
                yy = yy - controlFrame.top;
                int csize = controlFrame.width() / 3;
                xx = (int) xx / csize;
                yy = (int) yy / csize;
                touchMat = (int) (yy + (xx + yy) * (xx + yy + 1) / 2);
                //Log.d("Nav", "xx: " + xx + " yy: " + yy + " pairing: " + touchMat);
                switch (touchMat) {
                    case 0:
                        turnZccw();
                        moved = true;
                        break;
                    case 1:
                        turnXcw();
                        moved = true;
                        break;
                    case 2:
                        turnYccw();
                        moved = true;
                        break;
                    case 3:
                        turnZcw();
                        moved = true;
                        break;
                    case 4:
                        toGround();
                        moved = true;
                        break;
                    case 5:
                        turnZcw();
                        moved = true;
                        break;
                    case 7:
                        turnYcw();
                        moved = true;
                        break;
                    case 8:
                        turnXccw();
                        moved = true;
                        break;
                    case 12:
                        turnZccw();
                        moved = true;
                        break;
                    default:
                        break;
                }
            } else if (recField.contains((int) xx, (int) yy)) {
                moved = false;
                touchx = xx;
                touchy = yy;
                int xmin = gameValues.gameBoard.length, ymin = gameValues.gameBoard.length, xmax = 0, ymax = 0;
                for (int i = 0; i < gameValues.gamePiece.length; i++) {
                    xmin = Math.min(xmin, gameValues.gamePiece[i][0]);
                    ymin = Math.min(ymin, gameValues.gamePiece[i][1]);
                    xmax = Math.max(xmax, gameValues.gamePiece[i][0]);
                    ymax = Math.max(ymax, gameValues.gamePiece[i][1]);

                }
                int scale = 0;
                for (int s = 0; s < (9 - median(2)); s++) {
                    scale = scale + q[s];
                }
                float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
                xmin = (int) (recCornersBig[0] + scale + xmin * size);
                xmax = (int) (recCornersBig[0] + scale + (xmax + 1) * size);
                ymin = (int) (recCornersBig[1] + scale + ymin * size);
                ymax = (int) (recCornersBig[1] + scale + (ymax + 1) * size);
            }
        } else if (showDialog) {
            Random rand = new Random();
            if (rand.nextInt(100) <= 25) {
                AlertDialog.Builder builder = new Builder(context);
                builder.setTitle("Please Rate this app in the Google Play Store");
                builder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface di, int i) {
                                final String appPackageName = context
                                        .getPackageName(); // getPackageName() from
                                // Context or Activity
                                // object
                                try {
                                    context.startActivity(new Intent(
                                            Intent.ACTION_VIEW, Uri
                                            .parse("market://details?id="
                                                    + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    context.startActivity(new Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse("https://play.google.com/store/apps/details?id="
                                                    + appPackageName)));
                                }
                            }

                        });
                builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface di, int i) {
                                showDialog = false;
                            }
                        });
                builder.create().show();
            }
        }

    }

    public void debugButtons(Canvas canvas) {
        int xmin = gameValues.gameBoard.length, ymin = gameValues.gameBoard.length, xmax = 0, ymax = 0;
        for (int i = 0; i < gameValues.gamePiece.length; i++) {
            xmin = Math.min(xmin, gameValues.gamePiece[i][0]);
            ymin = Math.min(ymin, gameValues.gamePiece[i][1]);
            xmax = Math.max(xmax, gameValues.gamePiece[i][0]);
            ymax = Math.max(ymax, gameValues.gamePiece[i][1]);

        }
        int scale = 0;
        for (int s = 0; s < (9 - median(2)); s++) {
            scale = scale + q[s];
        }
        float size = ((Math.abs(recCornersBig[2] - recCornersBig[0]) - 2 * scale) / gameValues.gameBoard.length);
        xmin = (int) (recCornersBig[0] + scale + xmin * size);
        xmax = (int) (recCornersBig[0] + scale + (xmax + 1) * size);
        ymin = (int) (recCornersBig[1] + scale + ymin * size);
        ymax = (int) (recCornersBig[1] + scale + (ymax + 1) * size);
        basePaint.setAlpha(100);
        basePaint.setStyle(Style.FILL);
        basePaint.setColor(Color.RED);
        basePaint.setAlpha(100);
        canvas.drawRect(recCornersBig[0], ymin, xmin, ymax, basePaint);
        canvas.drawRect(xmin, recCornersBig[1], xmax, ymin, basePaint);
        canvas.drawRect(xmax, ymin, recCornersBig[2], ymax, basePaint);
        canvas.drawRect(xmin, ymax, xmax, recCornersBig[3], basePaint);
        canvas.drawCircle(touchx, touchy, 10, basePaint);
        basePaint.setAlpha(255);
        basePaint.setColor(Color.WHITE);
        canvas.drawText(String.valueOf(touchMat), width / 2, height / 2, basePaint);
        basePaint.setStyle(Style.STROKE);
    }

    private void frame(Canvas canvas) {
        height = canvas.getHeight();
        width = canvas.getWidth();
        int nwidth = neonv.getWidth() / 2;
        planesRect = new Rect(2, 2, height / 12, height - 3);
        recField = new Rect(planesRect.right, 0, width - height / 2, height);
        controlFrame = new Rect(recField.right, height - width + recField.right, width, height);
        Rect frame = new Rect(planesRect.left - nwidth / 2, 0, planesRect.left + nwidth / 2, height);
        canvas.drawBitmap(neonv, null, frame, basePaint);
        frame = new Rect(planesRect.right - nwidth / 2, 0, planesRect.right + nwidth / 2, height);
        canvas.drawBitmap(neonv, null, frame, basePaint);
        frame = new Rect(recField.right - nwidth / 2, 0, recField.right + nwidth / 2, height);
        canvas.drawBitmap(neonv, null, frame, basePaint);
        frame = new Rect(width - 2 - nwidth / 2, 0, width - 2 + nwidth / 2, height);
        canvas.drawBitmap(neonv, null, frame, basePaint);
        frame = new Rect(0, 2 - nwidth / 2, width, 2 + nwidth / 2);
        canvas.drawBitmap(neonh, null, frame, basePaint);
        frame = new Rect(0, height - 2 - nwidth / 2, width, height - 2 + nwidth / 2);
        canvas.drawBitmap(neonh, null, frame, basePaint);
        frame = new Rect(recField.right, height / 8 - nwidth / 2, width, height / 8 + nwidth / 2);
        canvas.drawBitmap(neonh, null, frame, basePaint);
        frame = new Rect(recField.right, height / 4 - nwidth / 2, width, height / 4 + nwidth / 2);
        canvas.drawBitmap(neonh, null, frame, basePaint);
        frame = new Rect(recField.right, controlFrame.top - nwidth / 2, width, controlFrame.top + nwidth / 2);
        canvas.drawBitmap(neonh, null, frame, basePaint);
        frame = new Rect((width + recField.right) / 2 - nwidth / 2, 0, (width + recField.right) / 2 + nwidth / 2, height / 4);
        canvas.drawBitmap(neonv, null, frame, basePaint);
    }

    public void setMedia(Context _context) {
        context = _context;
        WindowManager wm = (WindowManager) _context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        basePaint = new Paint();
        help = BitmapFactory.decodeResource(_context.getResources(),
                R.drawable.help_new);
        soundon = BitmapFactory.decodeResource(_context.getResources(),
                R.drawable.soundon);
        soundoff = BitmapFactory.decodeResource(_context.getResources(),
                R.drawable.soundoff);
        basePaint.setAntiAlias(true);
        basePaint.setFilterBitmap(true);
        basePaint.setDither(true);
        planesRect = new Rect(2, 2, height / 12, height - 3);
        //recField = new Rect(planesRect.right, 0, planesRect.right + height, height);
        recField = new Rect(planesRect.right, 0, width - height / 2, height);
        controlFrame = new Rect(recField.right, height - width + recField.right, width, height);
        neonh = BitmapFactory.decodeResource(_context.getResources(),
                R.drawable.neonh);
        neonv = BitmapFactory.decodeResource(_context.getResources(),
                R.drawable.neonv);
        soundinit(_context);
    }
}