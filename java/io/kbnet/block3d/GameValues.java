/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;


public class GameValues {
	public boolean[][][] gameBoard = null;
	public boolean playing = false;
	public long points = 0;
	public String highscores = "0,0,0;0,0,0;0,0,0";
	public String highscoreTimes = "0,0,0;0,0,0;0,0,0";
	public int speed = 1;
	public final static int[][][] Pieces = {
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 3, 1, 9 }, { 2, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 3, 1, 9 }, { 3, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 2, 2, 9 }, { 2, 3, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 2, 2, 9 }, { 3, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 1, 2, 9 }, { 2, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 3, 1, 9 }, { 2, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 3, 1, 9 }, { 3, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 2, 2, 9 }, { 2, 3, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 2, 2, 9 }, { 3, 2, 9 } },
			{ { 1, 1, 9 }, { 2, 1, 9 }, { 1, 2, 9 }, { 2, 2, 9 } },
			{ { 0, 1, 9 }, { 1, 1, 9 }, { 2, 1, 9 }, { 3, 1, 9 } },
			{ { 2, 2, 9 } }, { { 2, 2, 9 }, { 2, 3, 9 } } };
	public int[][] gamePiece = null;
	public int[][] nextgamePiece = null;
	public int stage;
	public int level;
	public long gameTime = 0;
	public boolean sound=true;
	public int levelup=0;

	public void updateScores() {
		String[] stagescore = highscores.split(";");
		String[] scores = (stagescore[stage-4].split(","));
		String[] stagetime = highscoreTimes.split(";");
		String[] times = (stagetime[stage-4].split(","));
		long time=(System.currentTimeMillis() - gameTime)/1000;
		
		if (points > Integer.parseInt(scores[0])) {
			scores[2] = scores[1];
			scores[1] = scores[0];
			scores[0] = "" + points;
			times[2] = times[1];
			times[1] = times[0];
			times[0] = ""+time;
			
		} else if (points > Integer.parseInt(scores[1])) {
			scores[2] = scores[1];
			scores[1] = "" + points;
			times[2] = times[1];
			times[1] = "" + time;
		} else if (points > Integer.parseInt(scores[2])) {
			scores[2] = "" + points;
			times[2] = "" + time;
		}
		if (stage == 4) {
			highscores = scores[0] + "," + scores[1] + "," + scores[2] + ";";
			String[] tempscores = (stagescore[1].split(","));
			highscores = highscores + tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2] + ";";
			tempscores = (stagescore[2].split(","));
			highscores = highscores + tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2];
			highscoreTimes = times[0] + "," + times[1] + "," + times[2] + ";";
			String[] temptimes = (stagetime[1].split(","));
			highscoreTimes = highscoreTimes + temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2] + ";";
			temptimes = (stagetime[2].split(","));
			highscoreTimes = highscoreTimes + temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2];
		} else if (stage==5) {
			String[] tempscores = (stagescore[0].split(","));
			highscores = tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2] + ";";
			highscores = highscores +scores[0] + "," + scores[1] + "," + scores[2] + ";";
			tempscores = (stagescore[2].split(","));
			highscores = highscores + tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2];
			
			String[] temptimes = (stagetime[0].split(","));
			highscoreTimes = temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2] + ";";
			highscoreTimes = highscoreTimes +times[0] + "," + times[1] + "," + times[2] + ";";
			temptimes = (stagetime[2].split(","));
			highscoreTimes = highscoreTimes + temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2];
		} else if (stage==6) {
			String[] tempscores = (stagescore[0].split(","));
			highscores = tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2] + ";";
			tempscores = (stagescore[1].split(","));
			highscores = highscores + tempscores[0] + "," + tempscores[1] + ","
					+ tempscores[2] + ";";
			highscores = highscores +scores[0] + "," + scores[1] + "," + scores[2];
			
			String[] temptimes = (stagetime[0].split(","));
			highscoreTimes = temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2] + ";";
			temptimes = (stagetime[1].split(","));
			highscoreTimes = highscoreTimes + temptimes[0] + "," + temptimes[1] + ","
					+ temptimes[2] + ";";
			highscoreTimes = highscoreTimes +times[0] + "," + times[1] + "," + times[2];
		}
	}

	public void set(int s, int l) {
		stage = s;
		level = l;
	}

	public int getStage() {
		return stage;
	}

	public int getLevel() {
		return level;
	}
}