/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity {
	private GameThread gameThread;
	private GameValues gameValues;
	private long pausetime = 0;
	private SharedPreferences prefs;
	private int startlevel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_game);
		gameValues = new GameValues();
		prefs = this.getSharedPreferences("io.kbnet.block3d",
				Context.MODE_PRIVATE);
		gameValues.highscores = prefs.getString("io.kbnet.block3d.highscores",
				"0,0,0;0,0,0;0,0,0");
		gameValues.highscoreTimes = prefs.getString(
				"io.kbnet.block3d.highscoreTimes", "0,0,0;0,0,0;0,0,0");
		gameValues.sound = prefs.getBoolean("sound", true);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			startlevel = extras.getInt("level", 1);
			gameValues.level = startlevel;
			gameValues.stage = extras.getInt("stage", 5);
		}
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
	}
	@Override
	protected void onStart() {
		super.onStart();
		startGame();
	}

	@Override
	protected void onPause() {
		super.onPause();
		gameValues = gameThread.getGameValues();
		pausetime = System.currentTimeMillis();
		Editor edit = prefs.edit();
		if (gameValues.playing) {
			edit.putString("io.kbnet.block3d.gameValues", gameValues.toString());
		} else {
			edit.putString("io.kbnet.block3d.gameValues", "");
		}
		edit.putString("io.kbnet.block3d.highscores", gameValues.highscores);
		edit.putString("io.kbnet.block3d.highscoreTimes",
				gameValues.highscoreTimes);
		edit.putBoolean("sound", gameValues.sound);
		edit.putInt("level",startlevel);
		edit.apply();
		// gameThread.pause(); // pause game when Activity pauses
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		boolean retry = true;
		Editor edit = prefs.edit();
		if (gameValues.playing) {
			edit.putString("io.kbnet.block3d.gameValues", gameValues.toString());
		} else {
			edit.putString("io.kbnet.block3d.gameValues", "");
		}
		edit.putString("io.kbnet.block3d.highscores", gameValues.highscores);
		edit.putString("io.kbnet.block3d.highscoreTimes",
				gameValues.highscoreTimes);
		edit.putBoolean("sound", gameValues.sound);
		edit.putInt("level",startlevel);
		edit.apply();
		if (gameThread != null) {
			gameThread.setRunning(false);
			while (retry) {
				try {
					gameThread.join();
					retry = false;
				} catch (InterruptedException ignored) {
				}
			}
			gameThread = null;
		}
	}

	private void startGame() {
		GameView gameView = findViewById(R.id.gameview);
		gameThread = gameView.getThread();
		gameThread.setRunning(true);
		gameThread.start();
		gameThread.setGameValues(gameValues);

		if (pausetime > 0)
			gameValues.gameTime = System.currentTimeMillis() - pausetime
					+ gameValues.gameTime;

	}
}

