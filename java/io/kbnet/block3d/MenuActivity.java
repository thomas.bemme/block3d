/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class MenuActivity extends Activity {
	private Button button1;
	private Button button2;
	private Button button3;
	private Button button4;
	private Button backButton;
	private TextView credits;
	private WebView highscoreDisp;
	private WebView howtoplayDisp;
	private int menulevel = 0;
	private SharedPreferences prefs;
	private String highscores;
	private String highscoreTimes;
	private int width;
	private int height;
	private TextView version;
	private PackageInfo pinfo;
	private Button minus;
	private Button plus;
	private TextView levelDisp;
	public int level = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_menu);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;
		height = metrics.heightPixels;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		prefs = this.getSharedPreferences("io.kbnet.block3d",
				Context.MODE_PRIVATE);
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		button4 = (Button) findViewById(R.id.button4);
		minus = (Button) findViewById(R.id.minus);
		plus = (Button) findViewById(R.id.plus);
		backButton = (Button) findViewById(R.id.backbutton);
		credits = (TextView) findViewById(R.id.textView1);
		version = (TextView) findViewById(R.id.version);
		levelDisp = (TextView) findViewById(R.id.level);
		highscoreDisp = (WebView) findViewById(R.id.textView2);
		howtoplayDisp = (WebView) findViewById(R.id.textView3);
		button1.setOnClickListener(radioButtonListener);
		button2.setOnClickListener(radioButtonListener);
		button3.setOnClickListener(radioButtonListener);
		button4.setOnClickListener(radioButtonListener);
		minus.setOnClickListener(radioButtonListener);
		plus.setOnClickListener(radioButtonListener);
		backButton.setOnClickListener(backListener);
		credits.setText(Html.fromHtml(getResources()
				.getString(R.string.credits)));
		credits.setMovementMethod(new ScrollingMovementMethod());
		credits.setMovementMethod(LinkMovementMethod.getInstance());
		if (pinfo != null) {
			version.setText("V" + pinfo.versionName);
		} else {
			version.setText("Error!");
		}
		version.setTextSize(version.getTextSize() / 2);
		ViewGroup.LayoutParams params = button1.getLayoutParams();
		params.width = width / 3;
		params.height = height / 10;
		button1.setLayoutParams(params);
		params = button2.getLayoutParams();
		params.width = width / 3;
		params.height = height / 10;
		button2.setLayoutParams(params);
		params = button3.getLayoutParams();
		params.width = width / 3;
		params.height = height / 10;
		button3.setLayoutParams(params);
		params = button4.getLayoutParams();
		params.width = width / 3;
		params.height = height / 10;
		button4.setLayoutParams(params);
		params = plus.getLayoutParams();
		params.width = width/9;
		plus.setLayoutParams(params);
		params = minus.getLayoutParams();
		params.width = width/9;
		minus.setLayoutParams(params);
		setMenu(0);

	}

	private float convertFromDp(int input) {
		final float scale = getResources().getDisplayMetrics().density;
		return ((input - 0.5f) / scale);
	}

	private void setMenu(int i) {
		menulevel = i;
		if (i == 1) {
			howtoplayDisp.setVisibility(View.GONE);
			credits.setVisibility(View.GONE);
			highscoreDisp.setVisibility(View.GONE);
			backButton.setVisibility(View.VISIBLE);
			button1.setVisibility(View.VISIBLE);
			button2.setVisibility(View.VISIBLE);
			button3.setVisibility(View.VISIBLE);
			minus.setVisibility(View.VISIBLE);
			plus.setVisibility(View.VISIBLE);
			levelDisp.setVisibility(View.VISIBLE);
			button4.setVisibility(View.INVISIBLE);
			button1.setText(getString(R.string.level1));
			button2.setText(getString(R.string.level2));
			button3.setText(getString(R.string.level3));
			plus.setText("+");
			minus.setText("-");
			levelDisp.setText("Level: " + level);

		} else if (i == 2) {
			howtoplayDisp.setBackgroundColor(0xAA000000);
			howtoplayDisp.loadUrl("file:///android_asset/index.html");
			howtoplayDisp.setVisibility(View.VISIBLE);
			highscoreDisp.setVisibility(View.GONE);
			backButton.setVisibility(View.VISIBLE);
			button1.setVisibility(View.INVISIBLE);
			button2.setVisibility(View.INVISIBLE);
			button3.setVisibility(View.INVISIBLE);
			button4.setVisibility(View.INVISIBLE);
			plus.setVisibility(View.INVISIBLE);
			minus.setVisibility(View.INVISIBLE);
			levelDisp.setVisibility(View.INVISIBLE);
		} else if (i == 3) {
			howtoplayDisp.setVisibility(View.GONE);
			credits.setVisibility(View.VISIBLE);
			highscoreDisp.setVisibility(View.GONE);
			backButton.setVisibility(View.VISIBLE);
			button1.setVisibility(View.INVISIBLE);
			button2.setVisibility(View.INVISIBLE);
			button3.setVisibility(View.INVISIBLE);
			button4.setVisibility(View.INVISIBLE);
			plus.setVisibility(View.INVISIBLE);
			minus.setVisibility(View.INVISIBLE);
			levelDisp.setVisibility(View.INVISIBLE);
		} else if (i == 4) {
			highscores = prefs.getString("io.kbnet.block3d.highscores",
					"0,0,0;0,0,0;0,0,0");
			highscoreTimes = prefs.getString("io.kbnet.block3d.highscoreTimes",
					"0,0,0;0,0,0;0,0,0");
			String[] highscoreArray = highscores.split(";");
			String[] highscoreTimesArray = highscoreTimes.split(";");
			String hO = "<table style=\"color:#9aeaff;font-size:normal\" align=right border=\"1\"CELLPADDING=3 CELLSPACING=1 RULES=ROWS FRAME=HSIDES><tr>";
			for (int k = 0; k < 3; k++) {
				hO += "<td><b>" + (k + 4) + "x" + (k + 4)
						+ "</b></td><td>Points</td><td>Time</td></tr>";
				for (int j = 0; j < 3; j++) {
					String score = "";
					String time = "";
					if (Integer.parseInt(highscoreArray[k].split(",")[j]) > 0) {
						score = highscoreArray[k].split(",")[j];
						time = String.format("%02d",
								(Integer.parseInt(highscoreTimesArray[k]
										.split(",")[j]) / 60))
								+ ":"
								+ String.format("%02d", (Integer
								.parseInt(highscoreTimesArray[k]
										.split(",")[j]) % 60));
					} else {
						score = "-";
						time = "-";
					}

					hO += "<tr><td>" + (j + 1) + ". </td><td>" + score
							+ "</td><td>" + time + "</td></tr>";
				}
			}
			hO += "</table>";
			highscoreDisp.loadDataWithBaseURL("", hO, "text/html", "UTF-8", "");
			highscoreDisp.setBackgroundColor(Color.TRANSPARENT);
			howtoplayDisp.setVisibility(View.GONE);
			highscoreDisp.setVisibility(View.VISIBLE);
			backButton.setVisibility(View.VISIBLE);
			button1.setVisibility(View.INVISIBLE);
			button2.setVisibility(View.INVISIBLE);
			button3.setVisibility(View.INVISIBLE);
			button4.setVisibility(View.INVISIBLE);
			plus.setVisibility(View.INVISIBLE);
			minus.setVisibility(View.INVISIBLE);
			levelDisp.setVisibility(View.INVISIBLE);

		} else {
			backButton.setVisibility(View.INVISIBLE);
			credits.setVisibility(View.GONE);
			howtoplayDisp.setVisibility(View.GONE);
			highscoreDisp.setVisibility(View.GONE);
			button1.setVisibility(View.VISIBLE);
			button2.setVisibility(View.VISIBLE);
			button3.setVisibility(View.VISIBLE);
			button4.setVisibility(View.VISIBLE);
			button1.setText(getString(R.string.start));
			button2.setText(getString(R.string.howtoplay));
			button3.setText(getString(R.string.about));
			button4.setText(getString(R.string.highscore));
			plus.setVisibility(View.INVISIBLE);
			minus.setVisibility(View.INVISIBLE);
			levelDisp.setVisibility(View.INVISIBLE);
			level = 1;

		}
	}

	@Override
	public void onBackPressed() {
		if (menulevel > 0) {
			setMenu(0);
		} else {
			super.onBackPressed();
		}
	}

	public OnClickListener radioButtonListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			Button Button = (Button) view;
			int id = Button.getId();
			if (menulevel == 0) {
				if (id == R.id.button1) {
					setMenu(1);
				} else if (id == R.id.button2) {
					howtoplay();
				} else if (id == R.id.button3) {
					credits();
				} else if (id == R.id.button4) {
					highscores();
				}
			} else if (menulevel == 1) {
				if (id == R.id.button1) {
					startGame(view, 4, level);
				} else if (id == R.id.button2) {
					startGame(view, 5, level);
				} else if (id == R.id.button3) {
					startGame(view, 6, level);
				} else if (id == R.id.plus) {
					levelup(true);
				} else if (id == R.id.minus) {
					levelup(false);
				}
			}
		}

		private void startGame(View view, int stage, int l) {
			Intent intent = new Intent(view.getContext(), GameActivity.class);
			intent.putExtra("stage", stage);
			intent.putExtra("level", l);
			startActivity(intent);

		}
	};

	private void levelup(boolean up) {
		if ((up) && (level < 10))
			level++;
		if ((!up) && (level > 1))
			level--;
		levelDisp.setText("Level: " + level);

	}

	private void credits() {
		setMenu(3);
	}

	protected void howtoplay() {
		setMenu(2);

	}

	protected void highscores() {
		setMenu(4);

	}

	public OnClickListener backListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			setMenu(0);

		}
	};
}
