/*
 * ******************************************************************************
 * Copyright (c) 2013, 2020, Blowfish Programming.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 * 3. All advertising materials mentioning features or use of this
 *      software must display the following acknowledgment:
 *      "This product includes software developed by Blowfish
 *      Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * 4. The name "Blowfish Programming" must not be used to
 *     endorse or promote products derived from this software without
 *     prior written permission. For written permission, please contact
 *     thomas.bemme@gmail.com
 *
 * 5. Redistributions of any form whatsoever must retain the following
 *     acknowledgment:
 *     "This product includes software developed by Blowfish
 *     Programming. (http://blowfishprogramming.wordpress.com/)"
 *
 * THIS SOFTWARE IS PROVIDED BYBLWOFISH PROGRAMMING "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 * *****************************************************************************
 */
package io.kbnet.block3d;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
    private static final long FPS = 10;
    private final SurfaceHolder _surfaceHolder;
    private final Context _context;
    private int width;
    private int height;
    private boolean isSurfaceCreated;
    private boolean isRunning;
    private boolean isPaused;
    private GameField gameField;

    private GameValues gameValues;


    public GameThread(SurfaceHolder surfaceHolder, Context context) {
        this._surfaceHolder = surfaceHolder;
        this._context = context;
        gameValues = new GameValues();
        init();
    }

    public void setSurfaceCreated(boolean surfaceCreated) {
        isSurfaceCreated = surfaceCreated;
    }

    public void setSurfaceSize(int width, int height) {
        // synchronized to make sure these all change atomically
        synchronized (_surfaceHolder) {
            if (width != this.width && height != this.height) {
                this.width = width;
                this.height = height;
            }
        }
    }

    public void setRunning(boolean b) {
        isRunning = b;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void pause() {
        synchronized (_surfaceHolder) {
            isPaused = true;
        }
    }

    public void unpause() {
        synchronized (_surfaceHolder) {
            isPaused = false;
        }
    }

    @Override
    public void run() {
        long ticksPS = 1000 / FPS;
        long startTime;
        long sleepTime;
        // wait for surface to become available
        while (!isSurfaceCreated && isRunning) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException ignored) {
            }
        }
        // Set the last tick to right now.
        while (isRunning) {
            while (isPaused && isRunning) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ignored) {
                }
                // coming out of pause, we don't want to jump ahead so we have
                // to set the last tick to now.
            }
            Canvas c = null;
            startTime = System.currentTimeMillis();
            try {
                synchronized (_surfaceHolder) {
                    c = _surfaceHolder.lockCanvas();
                    if (isSurfaceCreated) {
                        doDraw(c);
                    }
                }
            } catch (Throwable t) {
                throw new RuntimeException(t);
            } finally {
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
            }
            sleepTime = ticksPS - (System.currentTimeMillis() - startTime);
            try {
                if (sleepTime > 0)
                    sleep(sleepTime);
                else
                    sleep(10);
            } catch (Exception ignored) {
            }
        }
    }

    private void doDraw(Canvas canvas) {
        gameField.draw(canvas);
    }

    private void init() {
        gameField = new GameField();
        gameField.setMedia(_context);
    }

    public void releaseButton(float x, float y) {
        gameField.buttonRelease(x, y);
    }

    public void pressButton(float x, float y) {
        gameField.buttonPressed(x, y);
    }

    public void moveButton(float x, float y) {
        gameField.buttonMove(x, y);
    }

    public void setGameValues(GameValues gv) {
        gameValues = gv;
        gameField.create(gameValues);
    }

    public GameValues getGameValues() {
        return gameValues;
    }
}
